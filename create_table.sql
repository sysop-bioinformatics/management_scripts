-- Create database like this : cat create_table.sql | sqlite3 users.sqlite3.db
CREATE TABLE IF NOT EXISTS users
(
    ID          INTEGER PRIMARY KEY,
    ACCOUNT     TEXT NOT NULL,
    DEPARTMENT  TEXT NOT NULL,
    SUPERVISOR  TEXT,
    ADDED_BY    TEXT,
    ENTRY_DATE  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    END_DATE    DATETIME
);