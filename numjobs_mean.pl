#!/usr/bin/perl
use Time::Local;

my $seconds_in_day = 24 *   60 *    60;
my $total;
my $start;
my $now;
my ($month,$day,$year,$hour,$minute,$second);
my $elapsed_time;
my @line;
my @jobs_done;
my $days;
my $days_to_run;
while (1) {
	getthing();
	sleep 300;
}
sub getthing {
	my $total_jobs=1;
	my @qstat=`qstat -u jvh`;	
	foreach my $lines (@qstat) {
		@line = split(" ",$lines);
		# grab data from lines
		if (defined($line[7])) {
			if ( $line[7] =~ m/.*-(\d+):.*/ ){
				$total = $1;
				($month,$day,$year)= split("/",$line[5]);
				($hour,$minute,$second)= split(":",$line[6]);
			}
		}
		if (defined($line[9])) {
			if ( int($line[9]) ){
				push @jobs_done, int($line[9]);
			}
		}
	}
	# geometric mean
	foreach my $jobje (@jobs_done) {
		$total_jobs= $total_jobs * $jobje;
	}
	$number_of_jobs_running	= @jobs_done;
	my $geometric_mean	= ($total_jobs**(1/$number_of_jobs_running));
	
	my $jobsdone		=($geometric_mean-($number_of_jobs_running/2));
	
	$now			= time();
	$start			= timelocal($second,$minute,$hour,$day,($month-1),$year);
	$elapsed_time		= $now - $start;
	$days			= $elapsed_time/$seconds_in_day;
	$days_to_run		= ($total-$jobsdone)*($days/$jobsdone);
	
	open(OUTPUTFILE, ">>output.txt");
	print OUTPUTFILE time(),"\t",$days_to_run."\t";
	print OUTPUTFILE scalar localtime( time()+($days_to_run*$seconds_in_day) );
	print OUTPUTFILE "\t",$geometric_mean;
	print OUTPUTFILE "\n";
	close(OUTPUTFILE);
}
