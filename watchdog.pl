#!/usr/bin/perl -w

# 2007 December 4       Harm Nijveen
# Watchdog, a script to monitor processes and kill those that run too long
# Add process names to the $commandHash and users to the $user variable
# Can be run as an hourly cron job

use strict;

#location of the ps binary
my $PS = "/bin/ps";

#user(s) to check, add additional users separated by a comma
my $user = "www-data";

#hash with program names and max cpu times
#value program name, key time with format: hhmmss
my %commandHash = (
                        "clustalw" => 1000,
                        "clustal.csh" => 1000,
                        "iprscan" => 1000,
                        "edialign" => 1000,
                        "svgtree" => 1000,
                        "hmmtop" => 1000,
                        "dottup" => 1000,
                        "distmat" => 1000,
                        "fproml" => 1000,
                        "primersearch" => 1000,
                        "puttree" => 1000,
                        "drawtree" => 1000,
                        "primer3plus" => 1000,
                        "google.pl" => 1,
                        "primer3_core" => 1000,
                        "dotmatcher" => 1000,
                        "dotpath" => 1000,
                        "matcher" => 1000,
                        "geecee" => 100,
                        "retree" => 1000,
                        "seqmatchall" => 1000,
                        "palindrome" => 1000,
                        "seqret" => 1000,
                        "est2genome" => 1000,
                        "skipredundant" => 1000,
                        "fdnapenny" => 1000,
                        "formcon" => 1000,
                        "supermatcher" => 1000,
                        "einverted" => 1000,
                        "wordmatch" => 1000,
                        "diffseq" => 1000,
                        "muscle.csh" => 1000,
                        "muscle" => 1000,
                        "vrnasubopt" => 1000,
                        "stretcher" => 1000,
                        "polydot" => 1000,
                        "freak" => 1000,
                        "ffitch" => 1000,
                        "tacg" => 1000,
                        "etandem" => 1000,
                        "t_coffee" => 1000
);

my $commandline = "$PS -o pid,time,comm -u $user|";
open PS, $commandline or die "cannot run ps";
while (<PS>) {
        my ($pid,$time,$nTime,$comm);
        if(m/^ *(\d+) ([0-9-]*\d\d:\d\d:\d\d) (\S+)$/) {
                ($pid,$time,$comm) = ($1,$2,$3);
                $nTime = $time;
                $nTime =~ s/[:-]//g;
                if ($commandHash{$comm} && $nTime > $commandHash{$comm}) {
                        print "Killing pid $pid running command \"$comm\" with cpu time $time\n";
                        kill 15,$pid or die "Could not kill $pid\n";
                }
        }
}

close PS;
