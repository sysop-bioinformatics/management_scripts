#!/usr/bin/perl -w
use strict;
use Sys::Hostname;
use POSIX;
my $time=strftime("%F %T", localtime() );
my $rrdtool= "rrdtool";
my $epoch_seconds=time();
my %times=(
		"daily" => " ",
		"weekly" => "--start '-1wk'",
		"monthly" => "--start '-1mon'",
		"yearly" => "--start '-1yr'",
	);
my @files=();
my $searchtime;
my $total;
my @colors=(
	"#0000FF",
	"#FF0000",
	"#00FF00",
	"#000033",
	"#FF00B6",
	"#005300",
	"#FFD300",
	"#009FFF",
	"#9A4D42",
	"#00FFBE",
	"#783FC1",
	"#1F9698",
	"#FFACFD",
	"#B1CC71",
	"#F1085C",
	"#FE8F42",
	"#DD00FF",
	"#201A01",
	"#720055",
	"#766C95",
	"#02AD24",
	"#C8FF00",
	"#886C00",
	"#FFB79F",
	"#858567",
	"#A10300",
	"#14F9FF",
	"#00479E",
	"#DC5E93",
	"#93D4FF",
	"#004CFF"
	);
# get rrd files
opendir (DIR, ".") or die $!;
while (my $file = readdir(DIR)) {
	push (@files,$file) if ((-f $file) && ($file =~ m/\.rrd$/));
}
#loop trough times
foreach my $key(sort keys %times){
	$searchtime = (1*24*60)		if  ($key eq "daily");
	$searchtime = (7*24*60)		if  ($key eq "weekly");
	$searchtime = (30*24*60)	if  ($key eq "monthly");
	$searchtime = (365*24*60)	if  ($key eq "yearly");

	my $counter = 1;
	my $SUM_command="CDEF:TOTAL_SUM=";
	my $command = "$rrdtool graph ".$key.".png --title '".$key."@".hostname."' --vertical-label 'Percentage CPU' ".$times{$key};
	$command .= " --width 500 --height 250";
	$command .= " --alt-y-grid";
	#$command .= " --lazy";
	$command .= " --alt-autoscale";
	$command .= " --interlaced";
	$command .= " --imgformat PNG";
	# loop through files
	foreach my $file (sort @files) {
		my ($file_readtime) = (stat ($file))[9];
		if (($searchtime >= $epoch_seconds-$file_readtime)   ){
			#print $key,"\t",$file,"\t",($epoch_seconds-$file_readtime),"\n";

			my $vname = "X_".$file;
			my $file_short = $file;
			$vname =~ s/\.rrd$//;
			$file_short =~ s/\.rrd$//;
			$command .= " \\\nDEF:".$vname."=$file:cpu:AVERAGE";
			$command .= " \\\nCDEF:".$vname."_=".$vname.",UN,0,$vname,IF";
			$command .= " \\\nLINE2:".$vname."_".$colors[($counter % $#colors)].":$file_short";
			$SUM_command.=$vname."_,";
			#if ($counter == 1){
			#	$total = "CDEF:total_cpu=".$vname;
			#}else{
			#	$total .= ",".$vname.",+";
			#};
			$counter++;
		}
	}
	#$command .= " \\\nCOMMENT:\\\'".$time."\\\'";
	#$command .=  " \\\n".$total;
	#$command .= " \\\nAREA:total_cpu#339999:Total";
	#$command .= " \\\nGPRINT:total_cpu:Total\\ CPU\\ is\\ \%9.2lfA";
	chop($SUM_command);
	my @list=split(',', $SUM_command);
	$SUM_command .= ',+' x scalar(@list);
	$SUM_command = " \\\n".$SUM_command;
	#print "$SUM_command\n";
	#$command .= $SUM_command;
	system($command) == 0 or die "system $command failed: $?";
	#print $command."\n";
}
