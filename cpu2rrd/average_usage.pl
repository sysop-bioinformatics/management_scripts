#!/usr/bin/perl -w
use strict;
use warnings;
use diagnostics;

my %usage;
my $rounds = 5;
my $rrdtool= "rrdtool";

for (my $counter=0;$counter < $rounds;$counter++){
	add_usage(\%usage);
	sleep(1);
}

# Printout usage, divide by number of seconds
foreach my $key(sort keys %usage){
	my $filename = $key.".rrd";
	if ( !( (-e $filename) && (-w $filename) && (-f $filename) ) ){
		create_rrd($filename);
	}
	update_rrd($filename,$usage{$key}/$rounds);
}

# Subs
	sub add_usage{
		my $usage_hash = shift;
		my $ps = `ps -eo "pcpu,user"`; #get all processes, store in $ps
		my @all = split /\n/, $ps;  #split by newlines, put into @all array
		foreach my $proc (@all) {   #loop once for each item in @all array
			if ($proc !~ m/%CPU/){
				my ($percentage,$name)=split(" ",$proc);
				next if ($percentage == 0.0);
				${$usage_hash}{$name}+=$percentage;
			}
		}
	}

	sub create_rrd{
		my $filename = shift;
		my $command="$rrdtool create $filename DS:cpu:GAUGE:600:U:U RRA:AVERAGE:0.5:1:2000 RRA:AVERAGE:0.5:6:2000 RRA:AVERAGE:0.5:24:2000 RRA:AVERAGE:0.5:288:2000 RRA:MAX:0.5:1:2000 RRA:MAX:0.5:6:2000 RRA:MAX:0.5:24:2000 RRA:MAX:0.5:288:2000 RRA:MIN:0.5:1:2000 RRA:MIN:0.5:6:2000 RRA:MIN:0.5:24:2000 RRA:MIN:0.5:288:2000";
		system($command) == 0 or die "system $command failed: $?";
	}
	sub update_rrd{
		my $filename = shift;
		my $value = shift;
		my $command="$rrdtool update $filename N:$value";
		system($command) == 0 or die "system $command failed: $?";
	}
