#!/bin/bash
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"


set -o nounset
set -o errexit

if [ -z "${1-}" ] || [ -z "${SUDO_USER-}" ]
then
    echo "Usage : sudo $0 USERNAME"
    exit 1
fi

# Variables
NEW_USER=${1}
NEW_USER="${NEW_USER,,}"
ADMINPASSWORD=''
binddn=${SUDO_USER}@wurnet.nl
declare -A DN_ARRAY
#DN_ARRAY["dev1_Rusr"]="CN=SERVERS_dev1_Rusr,OU=dev1.ab,OU=BioInformatics,OU=NoPolicy,OU=Servers,DC=wurnet,DC=nl"
DN_ARRAY["AB"]="CN=USR_BIOINFORMATICS_SERVERS_AB_Rusr,OU=BioInformatics,OU=NoPolicy,OU=Servers,DC=wurnet,DC=nl"
DN_ARRAY["BIF"]="CN=USR_BIOINFORMATICS_SERVERS_BIF_Rusr,OU=BioInformatics,OU=NoPolicy,OU=Servers,DC=wurnet,DC=nl"
DN_ARRAY["BIS"]="CN=USR_BIOINFORMATICS_SERVERS_BIS_Rusr,OU=BioInformatics,OU=NoPolicy,OU=Servers,DC=wurnet,DC=nl"
DN_ARRAY["GEN"]="CN=USR_BIOINFORMATICS_SERVERS_GEN_Rusr,OU=BioInformatics,OU=NoPolicy,OU=Servers,DC=wurnet,DC=nl"
DN_ARRAY["HMI"]="CN=USR_BIOINFORMATICS_SERVERS_HMI_Rusr,OU=BioInformatics,OU=NoPolicy,OU=Servers,DC=wurnet,DC=nl"
DN_ARRAY["MOB"]="CN=USR_BIOINFORMATICS_SERVERS_MOB_Rusr,OU=BioInformatics,OU=NoPolicy,OU=Servers,DC=wurnet,DC=nl"
DN_ARRAY["NEM"]="CN=USR_BIOINFORMATICS_SERVERS_NEM_Rusr,OU=BioInformatics,OU=NoPolicy,OU=Servers,DC=wurnet,DC=nl"
DN_ARRAY["PPH"]="CN=USR_BIOINFORMATICS_SERVERS_PPH_Rusr,OU=BioInformatics,OU=NoPolicy,OU=Servers,DC=wurnet,DC=nl"
DN_ARRAY["VIR"]="CN=USR_BIOINFORMATICS_SERVERS_VIR_Rusr,OU=BioInformatics,OU=NoPolicy,OU=Servers,DC=wurnet,DC=nl"
DN_ARRAY["PHP"]="CN=USR_BIOINFORMATICS_SERVERS_PHP_Rusr,OU=BioInformatics,OU=NoPolicy,OU=Servers,DC=wurnet,DC=nl"
DN_ARRAY["WFBR"]="CN=USR_BIOINFORMATICS_SERVERS_WFBR_Rusr,OU=BioInformatics,OU=NoPolicy,OU=Servers,DC=wurnet,DC=nl"


DB="${SCRIPTPATH}"/users.sqlite3.db

STUDENT=''
SUPERVISOR=''
END_DATE=''
# Six months in the future, last day of that month
DEFAULT_END_DATE=$(date -d "`date +%Y%m01` +7  month -1 day" +%F)
# Functions

# https://gist.github.com/davejamesmiller/1965569
function ask() {
    local prompt default reply

    if [[ ${2:-} = 'Y' ]]; then
        prompt='Y/n'
        default='Y'
    elif [[ ${2:-} = 'N' ]]; then
        prompt='y/N'
        default='N'
    else
        prompt='y/n'
        default=''
    fi

    while true; do

        # Ask the question (not using "read -p" as it uses stderr not stdout)
        echo -n "$1 [$prompt] "

        # Read the answer (use /dev/tty in case stdin is redirected from somewhere else)
        read -r reply </dev/tty

        # Default?
        if [[ -z $reply ]]; then
            reply=$default
        fi

        # Check if the reply is valid
        case "$reply" in
            Y*|y*) return 0 ;;
            N*|n*) return 1 ;;
        esac

    done
}

function ad_id_to_dn() {
echo $(ldapsearch -x -LLL -E pr=200/noprompt -H ldaps://ldap.wurnet.nl -D "$binddn" -w "${ADMINPASSWORD}" "(&(samAccountName="$1"))" -b "dc=wurnet,dc=nl" dn | sed -e '/^$/,$d' | sed ':a;N;$!ba;s/\n //g'| awk '{$1=""; print $0}')
}

function ad_id_to_mail() {
  TERM=mail
  echo $(ldapsearch -x -LLL -E pr=200/noprompt -H ldaps://ldap.wurnet.nl -D "$binddn" -w "${ADMINPASSWORD}" "(&(samAccountName="$1"))" -b "dc=wurnet,dc=nl" $TERM | grep $TERM | cut -f 2 -d':')
}

function ad_id_to_name() {
  TERM=cn
  echo $(ldapsearch -x -LLL -E pr=200/noprompt -H ldaps://ldap.wurnet.nl -D "$binddn" -w "${ADMINPASSWORD}" "(&(samAccountName="$1"))" -b "dc=wurnet,dc=nl" $TERM | grep $TERM | cut -f 2 -d':') | awk -F, '{ print $2 " " $1 }' | sed 's/^ *//g'
}

function add_to_ad_group() {
  binddn=${SUDO_USER}@wurnet.nl
  NEW_USER=$1
  DN=$2
  local USERDN=$(ad_id_to_dn $NEW_USER)
  echo
  echo "Adding $NEW_USER to DN $DN in the AD."
ldapmodify -x -H ldaps://ldap.wurnet.nl -D "$binddn" -w "${ADMINPASSWORD}" << EOF
dn: $DN
changetype: modify
add: member
member: $USERDN
EOF
}

function check_id_in_ad_group() {
  binddn=${SUDO_USER}@wurnet.nl
  NEW_USER=$1
  DN=$2
  ldapsearch -LLL -H ldaps://ldap.wurnet.nl -x -b "DC=wurnet,DC=nl"  -D "$binddn" -w "${ADMINPASSWORD}" 'memberOf='"$DN" sAMAccountName | grep sAMAccountName| cut -f2 -d ':'  | grep -q $NEW_USER
  return $?
}

# Start of script

if [ -z "${ADMINPASSWORD-}" ]
then
    read -sp "Enter your password for ${SUDO_USER}:" ADMINPASSWORD
    echo
fi

# Default to No if the user presses enter without giving an answer:
if ask "Is $NEW_USER a student ?" N; then
    STUDENT="Y"
else
    STUDENT="N"
fi

if [ "$STUDENT" == "Y" ]
then 
    read -p "Who is the supervisor of $NEW_USER ? " SUPERVISOR
    SUPERVISOR="${SUPERVISOR,,}"
    read -p "What is the end date for access for $NEW_USER ? [${DEFAULT_END_DATE}] " END_DATE
    END_DATE=${END_DATE:-${DEFAULT_END_DATE}}
    echo
fi

PS3="Select a department to which $NEW_USER belongs: "

select DN_AVAIL in "${!DN_ARRAY[@]}"
do
    DN=${DN_ARRAY[$DN_AVAIL]}
    break
done

NAME=$(ad_id_to_name $NEW_USER)
MAIL=$(ad_id_to_mail $NEW_USER)
ADMIN_NAME=$(ad_id_to_name $SUDO_USER)

# If the user isn't in the list yet, add her
if check_id_in_ad_group $NEW_USER $DN
then
    echo
    echo -e "\e[31m$NEW_USER already in AD group $DN \033[0m"
else
    add_to_ad_group $NEW_USER $DN
fi

# If the folders don't exist yet, create them, and set the correct permissions
# 2023-03-07 in overleg met Harm Nijveen: niet standaard /mnt/scratch en /mnt/LTR_userdata aanmaken voor nieuwe users. Alleen op verzoek

# for dir in /mnt/scratch/${NEW_USER} /mnt/LTR_userdata/${NEW_USER} /lustre/BIF/nobackup/${NEW_USER}
for dir in /lustre/BIF/nobackup/${NEW_USER}
do
    if [ -d $dir ]
    then
        echo -e "\e[31m$dir already exists : \033[0m"
        ls -ld $dir
    else
        mkdir -p --mode=700 --verbose $dir
        chown --verbose ${NEW_USER}:'domain users' $dir
    fi
done
# Add user to database.
if [ -e "$DB" ]
then
    echo "Adding $NEW_USER to database $DB"
    sqlite3 -batch "$DB"  "insert into users (ACCOUNT,DEPARTMENT,ADDED_BY,SUPERVISOR,END_DATE) values ('"${NEW_USER}"','"${DN_AVAIL}"','"${SUDO_USER}"','"${SUPERVISOR}"','"${END_DATE}"');"
fi
# TODO mail this automatically, using the right return adress
echo "Use this as mail template to mail $MAIL :"
echo
cat << EOF
Hi $NAME,

You now have access to the shared servers of Bioinformatics.
These are shared with a couple of groups, these are :

- Bioscience, Applied Bioinformatics
- Chairgroup of Bioinformatics
- Chairgroup of Biosystematics
- Chairgroup of Genetics
- Chairgroup of Host-Microbe Interactomics
- Chairgroup of Molecular Bioology
- Chairgroup of Nematology
- Chairgroup of Plant Physiology
- Chairgroup of Virology
- Laboratory of Phytopathology
- Wageningen Food & Biobased Research

You should be able to log in to all our machines via ssh using you WUR account.
From a wired connection within the WUR all machines should be available directly,
from outside of the WUR or via WiFi you first need to log on to a machine that is connected to the internet,
for instance www.bioinformatics.nl (AKA myers.bioinformatics.nl) and then you can use SSH to connect to the other machines.

If you type a wrong password consecutively three times connecting to www.bioinformatics.nl you will be blocked for a day (manual override is possible),
so you should consider using ssh-key based authentication.

Your home directory is shared across all machines, it must contain no more than 10 GB.

The following network disk is available on (almost) all machines:
/lustre/BIF/nobackup/

(Lustre is also accessable on HPC/Anunna)

You have your own directory on these as well. Try to keep your data storage within reasonable limits.

The machines are shared resources, there are some rules for long running jobs:
Make sure to not claim a complete machine for a long time in terms of CPUs or memory without notice, we do not use a scheduling agent like on the HPC/Anunna.
Use nice -16 when starting a job.

The machines run Ubuntu. You can ask us to install some software if it is available in one of the repositories through apt, more ‘manual’ installations should be done by yourself.
A way to easily do this, is through bioconda : https://bioconda.github.io/. Conda installations should be done on the lustre filesystem

If you log on to a machine you usually get an overview of the load on the machines, and you can then choose a quiet one.
For questions and remarks it is best to use https://support.wur.nl, search for "IT Services for PSG Bioinformatics & Bioscience" as Service Offering.

Bye,
$ADMIN_NAME
EOF

