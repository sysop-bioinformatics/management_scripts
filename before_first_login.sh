#!/bin/bash
# By jan.vanhaarst@wur.nl

## Script to create home directory and make sure the cluster knows where to find it.
## Installation :
## /etc/pam.d/password-auth:
## session     optional      pam_exec.so stdout seteuid /root/before_first_login.sh

# Quit on error
set -o errexit

if [ $PAM_TYPE == 'open_session' ]
    then
    # Variables
    USER_NAME=${PAM_USER}
    HOME_LOCATION='/export/home/'${USER_NAME}
    PATH=$PATH:/opt/rocks/bin:/sbin
    HOMEDIRSRV=`rocks list host attr $(hostname -s)  | grep Info_HomeDirSrv | awk '{print $3}'`
    HOMEDIRLOC=`rocks list host attr $(hostname -s)  | grep Info_HomeDirLoc | awk '{print $3}'`
    # Check whether the home directory exists
    if [ $(grep -c '^'$USER_NAME /etc/auto.home) == 0 ]
    then
        # Create user entry in passwd file
        WBINFO=$(wbinfo --user-info $USER_NAME)
        if [ $(grep -c '^'$USER_NAME /etc/passwd) == 0 ]
        then
            echo "$WBINFO" >> /etc/passwd
        fi
        # Get user and group id's
        read USER_ID GROUP_ID <<< $(echo "$WBINFO" | awk -F':' '{print $3 " " $4}')
        # Fill home directory from /etc/skel
        if [ ! -d $HOME_LOCATION ]
        then
            cp -a /etc/skel $HOME_LOCATION
            chown -R $USER_ID:$GROUP_ID $HOME_LOCATION
        fi
        # Add user to auto.home and force automount to read it
        if [ $(grep -c '^'$USER_NAME /etc/auto.home) == 0 ]
        then
            echo -e "$USER_NAME\t${HOMEDIRSRV}:${HOMEDIRLOC}/${USER_NAME}" >> /etc/auto.home
                /sbin/service autofs restart
        fi
        # Tell Rocks of the new user
        rocks sync users > /dev/null
    fi
fi
