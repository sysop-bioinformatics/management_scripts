#!/usr/bin/perl -w
# this script splits a multifasta file in chunks

use strict;
use Bio::SeqIO;

my $sequence;
my $date 		= `date +%A_%d_%B_%Y_%Hh%Ms%S`;		# put formatted date into string
chop $date;							# remove linefeed from date command
my $teller		= 1;
my $length		= 0;
my $outputsize		= 100000000; 				# this is 100 Mbyte
my $size		= 0;
my $prefix		= "";
my $suffix		= ".fasta";
my $in  = Bio::SeqIO->new (
				-fh     => \*STDIN,		# Read data from STDIN
				-format => 'Fasta'		# Fasta in
				);

while ( my $seqobj = $in->next_seq() ) {
	my $id = $seqobj->display_id();
	my $outputfilename = $prefix.$teller.$suffix ;
	$id=~ s/[\|\.\ ]/_/g; 					# replace pipes and dots with underscores
	my $out = Bio::SeqIO->new (
				-file => ">>".$outputfilename  ,
				-format => 'Fasta' 		# Fasta out
				);
#	print "working on ".$seqobj->display_id()."\n";
	print "working on $outputfilename ($date)\n" if ($size == 0) ;
	my $length = $seqobj->length();
#	print "length of sequence is $length\n";
#	print "outputfile number: $teller\n";
	$out->write_seq($seqobj);
	$size	+= $length;
	if ($size >= $outputsize ){
	    print "$size\t$teller\n";
	    $teller++;
	    $size = 0;
	}	
}

