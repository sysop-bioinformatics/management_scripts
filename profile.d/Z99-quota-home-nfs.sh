#!/bin/bash
# Small script to retrieve quota usage and settings for a user.
# Get the real user ID
USERID=$(id -u `logname`)

USED=$(quota --no-wrap --human-readable --filesystem-list /home | grep home | awk '{print $2}')
SOFT=$(quota --no-wrap --human-readable --filesystem-list /home | grep home | awk '{print $3}')
echo -e "\nYou are using ${USED}B of your ${SOFT}B quota for your home folder,\nfor more info : quota -sf /home \n"
