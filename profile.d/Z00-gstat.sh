#!/bin/bash
# Script to show usage on other machines
# First get info from ganglia
HOST=lovelace.bioinformatics.nl
IP=$(dig $HOST +short | tail -1)
RESULT=$(timeout 0.5 gstat -al --gmond_ip=${IP} | sed '/localhost/,+1 d'  | sed 's/OFF//')
IGNORE='bunker|lovelace|scratch|craighead'
# If that is successfull, continue with printing the output
if [ -z "$RESULT" ]
then
    echo -
else
    echo
    echo "Usage on our other machines:"
    { echo Hostname,CPUs,Processes_running,Processes_total,load-1m,load-5m,load-15,cpu_user,cpu_nice,cpu_system,cpu_idle,cpu_wio | tr ',' ' ' ;echo "$RESULT"  | paste - - | tr '/' ' '| tr ',' ' '| tr -d '[]()' |  sort -k8n | grep -vE $IGNORE; } | column -t
fi
