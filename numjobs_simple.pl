#!/usr/bin/perl -w
use Time::Local;

my $seconds_in_day = 24 *   60 *    60;
my $total;
my $start;
my $now;
my ($month,$day,$year,$hour,$minute,$second);
my $elapsed_time;
my @line;
my $jobs_done;
my $days;
my $days_to_run;
my $total_jobs=1;
my @qstat=`qstat -u $ENV{USER}`;	
my $jobname	= $ENV{USER}."-vmatch";
foreach my $lines (@qstat) {
	@line = split(" ",$lines);
	# grab data from lines
	if (defined($line[2])) {
		if ( $line[2] =~ m/$jobname/ ) {
			if (defined($line[7])) {
				if ( $line[7] =~ m/.*-(\d+):.*/ ){
					($jobsdone,$total)	= split("-",$line[7]); # this should be done different
					$total			= $1;
					($month,$day,$year)= split("/",$line[5]);
					($hour,$minute,$second)= split(":",$line[6]);
				}
			}
		}
	}
}

$now			= time();
$start			= timelocal($second,$minute,$hour,$day,($month-1),$year);
$elapsed_time		= $now - $start;
$days			= $elapsed_time/$seconds_in_day;
$days_to_run		= ($total-$jobsdone)*($days/$jobsdone);
	
print time(),"\t",$days_to_run."\t";
print scalar localtime( time()+($days_to_run*$seconds_in_day) );
print "\t".scalar localtime( time() );
print "\t".$jobsdone;
print "\n";
