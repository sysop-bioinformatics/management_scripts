#!/usr/bin/perl -w
# This script finds specificity scores for output files generated in the primerdesign step.

#modules
	use strict;
	use File::Find;
	use File::Spec;
	use File::Basename;
	#use Unix::Processors;
	use Cwd;

#variables
	my $search_location;
	my $suffix		= ".prj";
	my $match		= "\\".$suffix."\$";
	my $vmatch_command_line	= "/home/jvh/vmatch/vmatch -complete -showdesc 0 -online -d -p -e 2 ";
	my $sequence;
	my $inputfilename;
	my $inputsequence;
	my $temp_dir		= -d '/tmp' ? '/tmp' : $ENV{TMP} || $ENV{TEMP};
	my @vmatch_result;
	my @data;
	my %oligohash;
	my ($genome,$position,$lenght,$mm);
	my $start = 0;
	my $run = 0;
	#my $procs = new Unix::Processors;
	#my $numprocs=$procs->max_online;
	my $numprocs=1;
	my $command_line;
	my @databases;
	my @filelist;
	my ($pid,$counter,$runs);
	my $minimal_length = 19;
	my $debug=0;
# walk tree with databases
	$search_location	= $ARGV[0];
	find({ wanted => \&findfile, follow => 1 },$search_location);			# File::Find won't let me add the array as a variable :-(
# put all files into array
	$filelist[0]	= $ARGV[1];

# Read datafile and put data into hash for later use
parse_file_to_hash($filelist[0], \%oligohash);
# run vmatch on databases, adding output to file
output_to_file($filelist[0], \%oligohash);

# Subs
	# to save space, compress the resulting file
	sub bzip2_file{
		my $file_ref	= shift;
		my $bzip2_command =  "bzip2 --quiet --best --compress $file_ref";
		my $bzip2_result = `$bzip2_command`;
		print STDERR $bzip2_result."\n";
	}
	# Open the file for output, and add the results of specificity_oligo() to it
	sub output_to_file{
		my $file_ref	= shift;
		my $hash_href	= shift;
		my $outputfile;
		my $number_of_databases = @databases;
		if ($number_of_databases == 1 ) {
			my ($databasevolume,$databasedirectories,$databasefile) = File::Spec->splitpath( $databases[0] );
			$outputfile	= $file_ref."_".$databasefile.".specificity";
		}else{
			$outputfile	= $file_ref.".specificity";
		}
		#print "$outputfile\n";
		open(OUTPUTFILE, ">>$outputfile");
		#print OUTPUTFILE "#/table=specificity\n";
		#print OUTPUTFILE "#oligo_id\tgenome\tposition\tlength\tmm\n";
		for my $oligo_id (keys %{$$hash_href{"$file_ref"}}) {
			#print STDERR $outputfile,":\t",$file_ref,"\t",$oligo_id,"\t",$$hash_href{"$file_ref"}{"$oligo_id"},"\n";
			specificity_oligo($oligo_id,$$hash_href{"$file_ref"}{"$oligo_id"}, *OUTPUTFILE);
		}
		close(OUTPUTFILE);
	}
	# parse the data from the file into a hash for further processing
	sub parse_file_to_hash{
		my $file_ref	= shift;
		my $hash_href	= shift;
		my ($oligo_id,$sequence);
		open(INPUTFILE, "<$file_ref") or die "Could not open $file_ref\n";
		print "working on $file_ref\n" if ($debug);

		while (<INPUTFILE>){
			$start = 1;
			if ($_ =~ /table=results/) {						# The next lines without # are the data I need.
				#$start = 1;
			}
			if( $_ !~ /#/ && $start == 1) {						# data found
				$run = 1;							# we are running a parserun
				my @data	= split(" ",$_);				# put the data in a hash
				my @number	= split("-",$data[1]);				# grab the number from data[1] to completely identify the oligo
				$oligo_id	= $data[0]." ".$number[0];			# this is the id of the oligo
				$sequence	= lc ($data[3]);				# use lowercase to store only one variant of LNA oligos
				if (length($sequence) >= $minimal_length){
					$$hash_href{"$file_ref"}{"$oligo_id"}= $sequence;
				}
				print "$oligo_id\t$sequence\n" if ($debug);
			}
			if ($_ =~ /^#/ && $run==1) {						# check whether we find another block of data
				print STDERR "end found!\n";
				last;
			}
		}
		close(INPUTFILE);
	}
	# run a vmatch analysis on an oligo using @databases
	sub specificity_oligo {
		my ($oligo_id, $inputsequence, $filehandle) = @_;
		# variables
		my $date 		= `date +%A_%d_%B_%Y_%Hh%Ms%S`;		# put formatted date into string
		chop $date;							# remove linefeed
		my $outputfilename	= "specificity_temp.".$date."_".$$;	# build output filename (date + pid)
		# Check input
		if ($inputsequence =~ m/[^actgACTG]/) {
			print STDERR "Faulty entry($inputsequence), aborting. \n";
			exit();
		}
		# create temporary file from input
		open(TEMPFILE, ">$temp_dir/$outputfilename");
		print ">$inputsequence\n$inputsequence\n" if ($debug);
		print TEMPFILE ">$inputsequence\n$inputsequence\n";
		close(TEMPFILE);
		print "tempfile created ($inputsequence) \n"  if ($debug);
		# Add file name to vmatchcommand
		$command_line = $vmatch_command_line ." -q $temp_dir/$outputfilename ";
		foreach my $file (@databases){
			vmatch($command_line.$file)
		}
		# print the resulting data in the format we want.
		foreach my $line (@vmatch_result){
			print "vmatch:\t$line" if ($debug);
			if ($line !~ /^#.*$/o){ 			# no match on regexp
				@data		= split(" ",$line);
				$genome		= $data[1];
				$position	= $data[2];
				$lenght		= $data[0];
				$mm		= $data[7];
				print $filehandle "$oligo_id\t"	if (defined($oligo_id));
				print $filehandle "$genome\t"	if (defined($genome));
				print $filehandle "$position\t"	if (defined($position));
				print $filehandle "$lenght\t"	if (defined($lenght));
				print $filehandle "$mm\n"	if (defined($mm));
			}
		}
		# cleanup
		unlink "$temp_dir/$outputfilename";
	}
	# Parse the filename to the databasename, and execute vmatch
	sub findfile {
			if ((/$match/o) ){
				my $filename	= $File::Find::name;
				my ($volume,$directories,$file) = File::Spec->splitpath( $filename );
				my $basename = basename($filename,$suffix);
				print "database $basename found.\n" if ($debug);
				push (@databases, File::Spec->catpath( $volume,$directories,$basename));
			}
	}
	# The wrapper around vmatch
	sub vmatch {
		my ($commandline) = @_;
		print $commandline."\n" if ($debug);
		@vmatch_result=();	# empty vmatch output
		push (@vmatch_result ,`$commandline`);
	}
	# generate a list of files in the current directory to be used as input
	sub filelist{
		my $array_href	= shift;
		my $cwd = cwd;
		my ($volume,$directory,$file) = File::Spec->splitpath( $cwd , 1 );
		@$array_href = glob( File::Spec->catpath( $volume,$directory,"*.txt") );
	}
