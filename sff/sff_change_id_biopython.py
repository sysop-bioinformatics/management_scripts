#!/usr/bin/python
""""
Script to append a suffix to an identifier.
see http://seqanswers.com/forums/showthread.php?p=8106
(c) Peter Cock, Jan van Haarst
"""

import sys
from optparse import OptionParser

sys.path = ["/home/jvh/code/biopython.nobackup/peterjc-biopython-fd061a56f32f16ae92fc8d076a371d869fb53c41"] + sys.path
#sys.path.append("/home/jvh/code/biopython.nobackup/peterjc-biopython-fd061a56f32f16ae92fc8d076a371d869fb53c41")

from Bio import SeqIO

def rename(record,suffix) :
	"""Function to alter the record's identifier."""
	record.id += suffix
	return record

def rename_split(record,suffix) :
	"""Function to alter the record's identifier."""
	record.id += record.id.split(suffix)[0]
	return record

def main():

	parser = OptionParser(usage="%prog --input=input_filename --output=output_filename --ID-suffix=string_to_add_to_identifier", version="%prog 1.0")

	parser.add_option("-i", "--input",
						dest="input_filename",
						help="Input filename",
						metavar="FILE")
	parser.add_option("-o", "--output",
						dest="output_filename",
						help="Output filename",
						metavar="FILE")
	parser.add_option("-s", "--ID-suffix",
						dest="suffix",
						help="String_to_add_to_identifier",
						metavar="STRING")

	(options, args) = parser.parse_args()

	if not options.output_filename or not options.input_filename or not options.suffix:
		parser.print_help()
		sys.exit(1)

	print 'Input:\t',	options.input_filename
	print 'Output:\t',	options.output_filename
	print 'Suffix:\t',	options.suffix

	#Python generator expression, only one record in memory at a time:
	records = (rename(rec,options.suffix) for rec in SeqIO.parse(open(options.input_filename,"rb"),"sff"))
	#records = (rename_split(rec,options.suffix) for rec in SeqIO.parse(open(options.input_filename,"rb"),"sff"))

	#This will not write the Roche XML manifest!
	handle = open(options.output_filename, "wb")
	SeqIO.write(records, handle, "sff")
	handle.close()

if __name__ == "__main__":
    main()

