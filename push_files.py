#!/usr/bin/python

#
# Execute this to push file to the temp directory
# on the first compute node and from there to the next, etc.
#

import os
import re
import sys

DEBUG = False

headNode = "apbioinf100.wurnet.nl"

scpCommand = "/usr/bin/scp"
sshCommand = "/usr/bin/ssh -x"

rsyncCommand = "/usr/bin/rsync -rtu --progress"

qhostCommand = "/opt/gridengine/bin/lx26-x86/qhost | grep compute"


sourceFiles = ""
destFiles = "/state/partition1/"
if (len(sys.argv) > 3):
    dirName = sys.argv[1]
    sourcePath = sys.argv[2]
    destPath = sys.argv[3]
else:
    print "Usage: push_files.py directoryName headNodeSourcePath computeNodeDestPath"
    print "Usage: push_files.py myDataFiles ~/data /state/partition1/ mydata"
    sys.exit(1)



# Generate the compute nodes list.
computeNodes = []
computeNodes.append(headNode)
child = os.popen(qhostCommand, "r")
for line in child.readlines():
    match = re.match("(compute-[0-9]-[0-9]+).*", line)
    if (match):
        computeNodes.append(match.group(1)+".local")
child.close()

if (DEBUG):
    print computeNodes


# Copy the files to the first node:

fromNode = ""
for toNode in computeNodes:
    if (fromNode == ""):
        fromNode = toNode
        continue
    if (fromNode == headNode):
        command = sshCommand + " " + fromNode + " \"" + rsyncCommand + " " + sourcePath +"/" + dirName  + " " + toNode + ":"+destPath + "\""
    else:
        command = sshCommand + " " + fromNode + " \"" + rsyncCommand + " " + destPath + "/" + dirName + " " + toNode + ":"+destPath + "\""

    if (DEBUG):
        print "command: " + command
    else:
        print "********** Copying from " + fromNode + " to " + toNode +"..."
        os.system(command)
        print "********** Done."

    fromNode = toNode

