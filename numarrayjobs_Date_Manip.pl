#!/usr/bin/perl
use Date::Manip;
# Get current time
$now=time;
# Get data from qstat
@lines=`qstat`;
foreach $line (@lines){
	# Find the arrayjob lines
	if ($line =~ m/\s\d+-\d+/){
		@words = split(" ",$line);
		# Grab the needed data from the line
		($save,$dummy)=split(":",$words[8]);
		($done,$total)=split("-",$save);
		$start = $words[5]." ".$words[6];
		# Parse the date into something useful
		$start = ParseDate($start);
		# Make it into seconds since the epoch
		$start = UnixDate($start,'%s');
		# Calculate how long we have to wait
		$seconds_left=($total-$done)/( $done/($now-$start) );
		# Round it, so we can use it.
		$seconds_left=int($seconds_left+.5);
		# Pretty print the time left.
		$delta = ParseDateDelta("0:".$seconds_left);
		print Delta_Format($delta, 3, "%hh"), "h ";
		print Delta_Format($delta, 1, "%md"), "m time left on $words[2] of $words[3]";
		# Pretty print the time when finished
		$end = ParseDateString("epoch ".($seconds_left+$now));
		$end = UnixDate($end,'%g');
		print " (done at ";
		print $end;
		print ")\n";
	}
}
