#!/bin/bash
printer="color_lj"
ip_adres="10.19.40.222"
echo "removing "$printer
lpadmin -x $printer 
echo "adding "$printer
lpadmin -p $printer -v /dev/null -m netstandard -o dest=$ip_adres:9100 -o protocol=tcp -o timeout=5 -I postscript -T PS -o banner=never
echo "changing filter for "$printer
cd /etc/lp/fd
for filter in *.fd;do
    name=`basename $filter .fd`
    lpfilter -f $name -F $filter
done 
echo "accepting "$printer
/usr/sbin/accept $printer
echo "enabling "$printer
/usr/bin/enable $printer
echo "showing "$printer
lpstat -p $printer
