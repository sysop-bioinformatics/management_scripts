#!/usr/bin/python
import imaplib
import getpass
import re

from operator import itemgetter
from optparse import OptionParser

# Nice formatting of bytes
def fmt3(num):
    for x in ['','Kb','Mb','Gb','Tb']:
        if num<1024:
	    return "%3.1f%s" % (num, x)
        num /=1024

# Extract mailbox from long_mailbox
def ExtractMailbox(mailbox_long):
	# Item: (\Marked \HasChildren) "/" INBOX
	Pattern = re.compile(r'^(\(.*\)).*(\".*\")\s+(.*)$')
	return Pattern.search(mailbox_long).group(3)

# Get options
parser = OptionParser()
parser.add_option("-s", "--server", dest="imap_server",action="store",
                  help="Server to query", default= "imap.wur.nl")
parser.add_option("-u", "--user", dest="user",action="store",
                  help="Username to use", default= "haars001")
parser.add_option("-P", "--pass", dest="password",action="store",
                  help="Username to use", default= "")
parser.add_option("-p", "--port", dest="imap_server_port",action="store",
                  help="Portnumber to use", type="int",default= "993")
parser.add_option("-q", action="store_false", dest="verbose")
parser.add_option("-v", action="store_true", dest="verbose",default= "true")
(options, args) = parser.parse_args()

imap_server = options.imap_server
user=options.user
imap_server_port=options.imap_server_port
verbose=options.verbose

if len(options.password)>0:
	password = options.password
else:
	password = getpass.getpass()

# Print connection
if verbose:
	print 'Using : ',user,'@',imap_server,':',imap_server_port

# Open a connection to the IMAP server
M = imaplib.IMAP4_SSL(imap_server,imap_server_port)
M.login(user,password)

# The list of all folders
result,mailbox_list = M.list()

if verbose:
	print "%-50s%5s%10s\n" % ("Folder", "# Msg", "Size")

number_of_messages_all = 0
size_all = 0
folder_sizes = {}

mailbox_list.sort(key=ExtractMailbox)

for mailbox_long in mailbox_list[:]:
	mailbox = ExtractMailbox(mailbox_long)
	if "gmail" in imap_server :
	    if "[Gmail]" in mailbox:
		    # Select the desired folder
		    result, number_of_messages  = M.select(mailbox, readonly=1)
		    if result=="OK":
			number_of_messages_all += int(number_of_messages[0])  # original line
			size_folder = 0
			# Go through all the messages in the selected folder
			typ, msg = M.search(None, 'ALL')
			# Find the first and last messages
			m = [int(x) for x in msg[0].split()]
			m.sort()
			if m:
				message_set = "%d:%d" % (m[0], m[-1])
				result, sizes_response = M.fetch(message_set, "(UID RFC822.SIZE)")
				for i in range(m[-1]):
				    tmp = sizes_response[i].split()
				    size_folder += int(tmp[-1].replace(')', ''))
			else:
				size_folder = 0
			if verbose:
				print "%-50s%5d%10s" % (mailbox, int(number_of_messages[0]), size_folder);
			folder_sizes[mailbox] = size_folder
			size_all += size_folder
	else:
	    if "Public Folders" not in mailbox:
		    # Select the desired folder
		    result, number_of_messages  = M.select(mailbox, readonly=1)
		    if result=="OK":
			number_of_messages_all += int(number_of_messages[0])  # original line
			size_folder = 0
			# Go through all the messages in the selected folder
			typ, msg = M.search(None, 'ALL')
			# Find the first and last messages
			m = [int(x) for x in msg[0].split()]
			m.sort()
			if m:
				message_set = "%d:%d" % (m[0], m[-1])
				result, sizes_response = M.fetch(message_set, "(UID RFC822.SIZE)")
				for i in range(m[-1]):
				    tmp = sizes_response[i].split()
				    size_folder += int(tmp[-1].replace(')', ''))
			else:
				size_folder = 0
			if verbose:
				print "%-50s%5d%10s" % (mailbox, int(number_of_messages[0]), size_folder);
			folder_sizes[mailbox] = size_folder
			size_all += size_folder

print "\n%-50s%5i%10s\n" % ("Sum", number_of_messages_all, fmt3(size_all))

if verbose:
	print "%-50s%10s\n" % ("Folder", "Size")
	for folder_size in  sorted(folder_sizes.items(), key=itemgetter(1)):
		print "%-50s%10s" % (folder_size[0], fmt3(folder_size[1]));
# Close the connection
M.logout()

