#!/usr/bin/bash
PASSWORD="pw4kwatta"
LOCATION="/home/jvh/data/backup"

for name in $(/usr/local/mysql-max-4.1.11-sun-solaris2.9-sparc-64bit/bin/mysqlshow -u root --password=$PASSWORD | grep -v "+" | grep -v "Databases" | awk '{print $2}'); do
    /usr/local/mysql-max-4.1.11-sun-solaris2.9-sparc-64bit/bin/mysqldump \
	--opt --allow-keywords --create-options --disable-keys --extended-insert \
        --comments --complete-insert --lock-tables --user=root --password=$PASSWORD $name \
        | bzip2 -9 > "$LOCATION"/`date +\%A`.`hostname`"."$name"_data".mysql.bz2
    /usr/local/mysql-max-4.1.11-sun-solaris2.9-sparc-64bit/bin/mysqldump \
	--no-data \
        --opt --allow-keywords --create-options --disable-keys --extended-insert \
        --comments --complete-insert --lock-tables --user=root --password=$PASSWORD $name \
	| bzip2 -9 > "$LOCATION"/`date +\%A`.`hostname`"."$name"_structure_".mysql.bz2
done