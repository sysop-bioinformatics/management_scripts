#!/bin/bash
# This script casts a directory of choice using udpcast.
# Usage : udpcast.sh directory/to/be/transferred/
# The script will automagically turn relative paths into absolute ones.
# The files will be extracted in the same location as where they were tarred.
# Needed :
#  udp-receiver
#  udp-sender
#  gnu tar
#  ssh
#  cluster-fork from Rocks Linux Cluster Software
#  rsync

# Written by Jan van Haarst (Plant Research International), last change 28/04/2005

DIR=$(cd "$1" && pwd) # maybe better : http://cclib.nsu.ru/projects/gnudocs/gnudocs/tar/tar_102.html#SEC97
echo "Source directory : $DIR"
echo -n "Is this OK (Y/n)? "
read yes_no
if [ "$yes_no" = "y" ] || [ "$yes_no" = "Y" ] || [ -z "$yes_no" ]
then
	# turn on the receivers on all nodes
	cluster-fork --bg "/home/jvh/bin/udp-receiver --pipe 'tar -x -C / -f -' --nokbd >/dev/null 2>&1"
	# turn on the sender on the frontend

	/home/jvh/bin/udp-sender --nopointopoint --full-duplex --min-clients 26 --min-wait 0 --max-wait 60 --nokbd --pipe "tar -cf - $DIR"
	# maybe we need to tweak these settings a bit more.

	# now the files should be transferred, to be sure we now synchronize the two directories:
	# possible options :
	# unison ( http://web.bii.a-star.edu.sg/~francis/Unison/ )
	# rsync  ( http://samba.anu.edu.au/rsync/ )
	# zsync  ( http://zsync.moria.org.uk/ )
	# cook up something ourselves
	# The easiest solution is to use rsync over ssh , as that is already installed and is very easy to use:
	cluster-fork rsync --archive --delete --verbose -e ssh `hostname`:$DIR `dirname $DIR`

	# done
	exit
else
	echo "OK, you type something else then \"Y\" so script terminated!!."
	exit
fi
