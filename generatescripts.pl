#!/usr/bin/perl -w
# ls -1 *.txt | sed 's/txt//' | xargs -t -i split --lines 1000 {}txt {}

@filename=reverse( glob("/home/jvh/results/split/*"));

#push (@databases,"/home/jvh/data/data_from_kwatta/data/bacteria/newsplit/1/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/bacteria/newsplit/2/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/bacteria/newsplit/3/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/bacteria/newsplit/4/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/bacteria/newsplit/5/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/bacteria/newsplit/6/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/bacteria/newsplit/7/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/bacteria/newsplit/8/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/bacteria/newsplit/9/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/bacteria/newsplit/10/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/bacteria/newsplit/11/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/bacteria/newsplit/12/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/bacteria/newsplit/13/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/bacteria/newsplit/14/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/bacteria/newsplit/15/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/fungi/1/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/fungi/2/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/fungi/3/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/fungi/4/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/fungi/5/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/fungi/6/");
#push (@databases,"/home/jvh/data/data_from_kwatta/data/virus/");

push (@databases,"/state/partition1/jvh/bacteria/newsplit/1/");
push (@databases,"/state/partition1/jvh/bacteria/newsplit/2/");
push (@databases,"/state/partition1/jvh/bacteria/newsplit/3/");
push (@databases,"/state/partition1/jvh/bacteria/newsplit/4/");
push (@databases,"/state/partition1/jvh/bacteria/newsplit/5/");
push (@databases,"/state/partition1/jvh/bacteria/newsplit/6/");
push (@databases,"/state/partition1/jvh/bacteria/newsplit/7/");
push (@databases,"/state/partition1/jvh/bacteria/newsplit/8/");
push (@databases,"/state/partition1/jvh/bacteria/newsplit/9/");
push (@databases,"/state/partition1/jvh/bacteria/newsplit/10/");
push (@databases,"/state/partition1/jvh/bacteria/newsplit/11/");
push (@databases,"/state/partition1/jvh/bacteria/newsplit/12/");
push (@databases,"/state/partition1/jvh/bacteria/newsplit/13/");
push (@databases,"/state/partition1/jvh/bacteria/newsplit/14/");
push (@databases,"/state/partition1/jvh/bacteria/newsplit/15/");
push (@databases,"/state/partition1/jvh/fungi/1/");
push (@databases,"/state/partition1/jvh/fungi/2/");
push (@databases,"/state/partition1/jvh/fungi/3/");
push (@databases,"/state/partition1/jvh/fungi/4/");
push (@databases,"/state/partition1/jvh/fungi/5/");
push (@databases,"/state/partition1/jvh/fungi/6/");
push (@databases,"/state/partition1/jvh/virus/");

$teller=1;

foreach $file (@filename) {
	foreach $database (@databases) {
		print $teller."\n";
		open(OUTPUTFILE, ">$teller");
		print OUTPUTFILE "#!/bin/bash\n";
		print OUTPUTFILE "/home/jvh/specificity.pl ".$database." ".$file."\n";
		close(OUTPUTFILE);
		chmod(oct(755), $teller);
		$teller++;
	}
}

