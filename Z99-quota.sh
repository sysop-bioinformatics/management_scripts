#!/bin/bash
# Small script to retrieve quota usage and settings for a user.
#set -o verbose
#set -o xtrace
# Get the real user ID
USERID=$(id -u `logname`)

USED=`/usr/sbin/xfs_quota -x -c "report -N -h -L $USERID -U $USERID" 2>/dev/null| grep '^#'|awk '{print $2}'`
SOFT=`/usr/sbin/xfs_quota -x -c "report -N -h -L $USERID -U $USERID" 2>/dev/null| grep '^#'|awk '{print $3}'`
echo -e "\nYou are using ${USED}B of your ${SOFT}B quota in your home folder\n"
