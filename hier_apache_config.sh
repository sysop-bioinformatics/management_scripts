#!/usr/bin/bash
#Apache install:
./configure			\
--prefix=/usr/local/apache2	\
--enable-auth-dbm		\
--enable-headers		\
--enable-proxy         		\
--enable-proxy-connect 		\
--enable-proxy-ftp		\
--enable-proxy-http		\
--enable-http			\
--enable-ftp			\
--enable-cgi			\
--enable-so			\

#--sbindir=/sbin		\
#--libdir=/local/lib		\
#--includedir=/local/include	\
#--exec-prefix=/usr/local	\
#--bindir=/usr/local/bin	\

#--enable-ssl			\
# ./configure --help
#`configure' configures this package to adapt to many kinds of systems.
#
#Usage: ./configure [OPTION]... [VAR=VALUE]...
#
#To assign environment variables (e.g., CC, CFLAGS...), specify them as
#VAR=VALUE.  See below for descriptions of some of the useful variables.
#
#Defaults for the options are specified in brackets.
#
#Configuration:
#  -h, --help              display this help and exit
#      --help=short        display options specific to this package
#      --help=recursive    display the short help of all the included packages
#  -V, --version           display version information and exit
#  -q, --quiet, --silent   do not print `checking...' messages
#      --cache-file=FILE   cache test results in FILE [disabled]
#  -C, --config-cache      alias for `--cache-file=config.cache'
#  -n, --no-create         do not create output files
#      --srcdir=DIR        find the sources in DIR [configure dir or `..']
#
#Installation directories:
#  --prefix=PREFIX         install architecture-independent files in PREFIX
#                          [/usr/local/apache2]
#  --exec-prefix=EPREFIX   install architecture-dependent files in EPREFIX
#                          [PREFIX]
#
#By default, `make install' will install all the files in
#`/usr/local/apache2/bin', `/usr/local/apache2/lib' etc.  You can specify
#an installation prefix other than `/usr/local/apache2' using `--prefix',
#for instance `--prefix=$HOME'.
#
#For better control, use the options below.
#
#Fine tuning of the installation directories:
#  --bindir=DIR           user executables [EPREFIX/bin]
#  --sbindir=DIR          system admin executables [EPREFIX/sbin]
#  --libexecdir=DIR       program executables [EPREFIX/libexec]
#  --datadir=DIR          read-only architecture-independent data [PREFIX/share]
#  --sysconfdir=DIR       read-only single-machine data [PREFIX/etc]
#  --sharedstatedir=DIR   modifiable architecture-independent data [PREFIX/com]
#  --localstatedir=DIR    modifiable single-machine data [PREFIX/var]
#  --libdir=DIR           object code libraries [EPREFIX/lib]
#  --includedir=DIR       C header files [PREFIX/include]
#  --oldincludedir=DIR    C header files for non-gcc [/usr/include]
#  --infodir=DIR          info documentation [PREFIX/info]
#  --mandir=DIR           man documentation [PREFIX/man]
#
#System types:
#  --build=BUILD     configure for building on BUILD [guessed]
#  --host=HOST       cross-compile to build programs to run on HOST [BUILD]
#  --target=TARGET   configure for building compilers for TARGET [HOST]
#
#Optional Features:
#  --disable-FEATURE       do not include FEATURE (same as --enable-FEATURE=no)
#  --enable-FEATURE[=ARG]  include FEATURE [ARG=yes]
#  --enable-layout=LAYOUT
#  --enable-v4-mapped      Allow IPv6 sockets to handle IPv4 connections
#  --enable-maintainer-mode
#                          Turn on debugging and compile time warnings
#  --enable-modules=MODULE-LIST
#                          Modules to enable
#  --enable-mods-shared=MODULE-LIST
#                          Shared modules to enable
#  --disable-access        host-based access control
#  --disable-auth          user-based access control
#  --enable-auth-anon      anonymous user access
#  --enable-auth-dbm       DBM-based access databases
#  --enable-auth-digest    RFC2617 Digest authentication
#  --enable-isapi          isapi extension support
#  --enable-file-cache     File cache
#  --enable-echo           ECHO server
#  --disable-charset-lite  character set translation
#  --enable-charset-lite   character set translation
#  --enable-cache          dynamic file caching
#  --enable-disk-cache     disk caching module
#  --enable-mem-cache      memory caching module
#  --enable-example        example and demo module
#  --enable-case-filter    example uppercase conversion filter
#  --enable-case-filter-in example uppercase conversion input filter
#  --enable-ldap           LDAP caching and connection pooling services
#  --enable-auth-ldap      LDAP based authentication
#  --enable-ext-filter     external filter module
#  --disable-include       Server Side Includes
#  --enable-deflate        Deflate transfer encoding support
#  --disable-log-config    logging configuration
#  --enable-logio          input and output logging
#  --disable-env           clearing/setting of ENV vars
#  --enable-mime-magic     automagically determining MIME type
#  --enable-cern-meta      CERN-type meta files
#  --enable-expires        Expires header control
#  --enable-headers        HTTP header control
#  --enable-usertrack      user-session tracking
#  --enable-unique-id      per-request unique ids
#  --disable-setenvif      basing ENV vars on headers
#  --enable-proxy          Apache proxy module
#  --enable-proxy-connect  Apache proxy CONNECT module
#  --enable-proxy-ftp      Apache proxy FTP module
#  --enable-proxy-http     Apache proxy HTTP module
#  --enable-ssl            SSL/TLS support (mod_ssl)
#  --enable-optional-hook-export
#                          example optional hook exporter
#  --enable-optional-hook-import
#                          example optional hook importer
#  --enable-optional-fn-import
#                          example optional function importer
#  --enable-optional-fn-export
#                          example optional function exporter
#  --enable-bucketeer      buckets manipulation filter
#  --enable-static-support Build a statically linked version the support
#                          binaries
#  --enable-static-htpasswd
#                          Build a statically linked version of htpasswd
#  --enable-static-htdigest
#                          Build a statically linked version of htdigest
#  --enable-static-rotatelogs
#                          Build a statically linked version of rotatelogs
#  --enable-static-logresolve
#                          Build a statically linked version of logresolve
#  --enable-static-htdbm   Build a statically linked version of htdbm
#  --enable-static-ab      Build a statically linked version of ab
#  --enable-static-checkgid
#                          Build a statically linked version of checkgid
#  --enable-http           HTTP protocol handling
#  --disable-mime          mapping of file-extension to MIME
#  --enable-dav            WebDAV protocol handling
#  --disable-status        process/thread monitoring
#  --disable-autoindex     directory listing
#  --disable-asis          as-is filetypes
#  --enable-info           server information
#  --enable-suexec         set uid and gid for spawned processes
#  --disable-cgid          CGI scripts
#  --enable-cgi            CGI scripts
#  --disable-cgi           CGI scripts
#  --enable-cgid           CGI scripts
#  --enable-dav-fs         DAV provider for the filesystem
#  --enable-vhost-alias    mass hosting module
#  --disable-negotiation   content negotiation
#  --disable-dir           directory request handling
#  --disable-imap          internal imagemaps
#  --disable-actions       Action triggering on requests
#  --enable-speling        correct common URL misspellings
#  --disable-userdir       mapping of user requests
#  --disable-alias         translation of requests
#  --enable-rewrite        regex URL translation
#  --enable-so             DSO capability
#
#Optional Packages:
#  --with-PACKAGE[=ARG]    use PACKAGE [ARG=yes]
#  --without-PACKAGE       do not use PACKAGE (same as --with-PACKAGE=no)
#  --with-apr=DIR|FILE     prefix for installed APR, path to APR build tree,
#                          or the full path to apr-config
#  --with-apr-util=DIR     prefix for installed APU, or path to APU build tree
#  --with-port=PORT        Port on which to listen (default is 80)
#  --with-z=DIR            use a specific zlib library
#  --with-ssl=DIR          SSL/TLS toolkit (OpenSSL)
#  --with-mpm=MPM          Choose the process model for Apache to use.
#                          MPM={beos|worker|prefork|mpmt_os2|perchild|leader|threadpool}
#  --with-module=module-type:module-file
#                          Enable module-file in the modules/<module-type>
#                          directory.
#  --with-program-name     alternate executable name
#  --with-suexec-bin       Path to suexec binary
#  --with-suexec-caller    User allowed to call SuExec
#  --with-suexec-userdir   User subdirectory
#  --with-suexec-docroot   SuExec root directory
#  --with-suexec-uidmin    Minimal allowed UID
#  --with-suexec-gidmin    Minimal allowed GID
#  --with-suexec-logfile   Set the logfile
#  --with-suexec-safepath  Set the safepath
#  --with-suexec-umask     umask for suexec'd process
#
#Some influential environment variables:
#  CC          C compiler command
#  CFLAGS      C compiler flags
#  LDFLAGS     linker flags, e.g. -L<lib dir> if you have libraries in a
#              nonstandard directory <lib dir>
#  CPPFLAGS    C/C++ preprocessor flags, e.g. -I<include dir> if you have
#              headers in a nonstandard directory <include dir>
#  CPP         C preprocessor
#
#Use these variables to override the choices made by `configure' or to help
#it to find libraries and programs with nonstandard names/locations.
#
