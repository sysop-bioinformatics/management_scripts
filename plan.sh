#!/bin/bash
# Small program to check for a .plan file, and create it if it doesn't.
PLAN=~/.plan
AD_group_ID=16777729

if [ ! -s $PLAN ] && [ "`id -g`" == ${AD_group_ID} ]
then
    echo -n "Please enter your full name [ENTER]: "
    read fullname
    echo -n "Please enter the name of your supervisor and press [ENTER]: "
    read RP
    echo "Fullname: " "${fullname}" > $PLAN
    echo "Supervisor: " "${RP}" >> $PLAN
    echo "Information stored in ${PLAN}, please update if changed in the future"
fi
